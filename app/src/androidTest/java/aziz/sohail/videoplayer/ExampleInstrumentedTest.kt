package aziz.sohail.videoplayer

import android.net.Uri
import android.util.Log
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("aziz.sohail.videoplayer", appContext.packageName)
    }

    @Test
    fun testUri() {

        val videoUrl = "https://tech-challenge-mobile.s3.eu-central-1.amazonaws.com/0028/0028.mp4"
        val thumb = "0028T.jpg"

        val uri = Uri.parse(videoUrl)
        val lastPathSegment= uri.lastPathSegment


        System.out.println("sohaila uri=" + uri.toString())
        System.out.println("sohaila path=" + uri.path.toString())
        System.out.println("sohaila lastPath=" + uri.lastPathSegment.toString())


        val thumbUrl=videoUrl.replace(lastPathSegment!!,thumb)

        System.out.println("sohaila thumbUrl="+thumbUrl.toString())


    }
}
