package aziz.sohail.videoplayer

import android.net.Uri
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun testUri() {

        val videoUrl = "https://tech-challenge-mobile.s3.eu-central-1.amazonaws.com/0028/0028.mp4"
        val thumb="0028T.jpg"

        val uri = Uri.parse(videoUrl)

        System.out.println("uri="+uri.toString())
        System.out.println("path=" + uri.path)
        System.out.println("lastPath=" + uri.lastPathSegment)

    }
}
