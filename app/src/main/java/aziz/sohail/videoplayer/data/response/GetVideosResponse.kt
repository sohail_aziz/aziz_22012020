package aziz.sohail.videoplayer.data.response

import aziz.sohail.videoplayer.data.entity.VideoEntity
import com.squareup.moshi.Json

data class GetVideosResponse(
    @Json(name = "videos")
    val videos: List<VideoEntity>
)