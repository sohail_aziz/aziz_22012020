package aziz.sohail.videoplayer.data

import aziz.sohail.videoplayer.data.datastore.local.IMockDataStore
import aziz.sohail.videoplayer.data.mapper.VideoEntityToVideoModelMapper
import aziz.sohail.videoplayer.domain.IVideoRepository
import aziz.sohail.videoplayer.domain.model.VideoModel
import io.reactivex.Single
import javax.inject.Inject

class VideoRepository
@Inject constructor(
    private val localDataStore: IMockDataStore,
    private val videoEntityToVideoMapper: VideoEntityToVideoModelMapper
) : IVideoRepository {

    override fun getVideos(): Single<List<VideoModel>> {

        return localDataStore.getVideoList()
            .map {
                videoEntityToVideoMapper.map(it)
            }
    }


}