package aziz.sohail.videoplayer.data.datastore.local

import aziz.sohail.videoplayer.data.entity.VideoEntity
import aziz.sohail.videoplayer.data.response.GetVideosResponse
import com.squareup.moshi.Moshi
import io.reactivex.Single
import javax.inject.Inject

class MockDataStore
@Inject constructor(
    private val moshi: Moshi
) : IMockDataStore {

    override fun getVideoList(): Single<List<VideoEntity>> {

        val adapter = moshi.adapter(GetVideosResponse::class.java)
        val videos = adapter.fromJson(GET_VIDEO_JSON)
            ?.videos

        return Single.just(videos)
    }


    companion object {

        const val GET_VIDEO_JSON = ""
    }
}