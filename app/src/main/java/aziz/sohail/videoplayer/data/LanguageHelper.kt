package aziz.sohail.videoplayer.data

import android.content.Context
import android.os.Build
import javax.inject.Inject


class LanguageHelper
@Inject constructor(
    context: Context
) {

    val currentLanguageCode: String =

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            context.resources.configuration.locales[0].language
        } else {
            @Suppress("DEPRECATION")
            context.resources.configuration.locale.language
        }
}

