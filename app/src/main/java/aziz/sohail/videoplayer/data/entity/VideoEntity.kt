package aziz.sohail.videoplayer.data.entity

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class VideoEntity(
    @Json(name = "video_url")
    val videoUrl: String,
    @Json(name = "audio_urls")
    val audioUrls: Map<String, String>,
    @Json(name = "thumb")
    val thumbnail: String,
    @Json(name = "title")
    val title: String
)