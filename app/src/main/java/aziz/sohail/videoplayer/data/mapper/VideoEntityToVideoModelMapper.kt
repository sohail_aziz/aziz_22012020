package aziz.sohail.videoplayer.data.mapper

import android.net.Uri
import aziz.sohail.videoplayer.Settings
import aziz.sohail.videoplayer.data.LanguageHelper
import aziz.sohail.videoplayer.data.entity.VideoEntity
import aziz.sohail.videoplayer.domain.model.VideoModel
import javax.inject.Inject

class VideoEntityToVideoModelMapper
@Inject constructor(
    private val languageHelper: LanguageHelper
) {

    private fun map(videoEntity: VideoEntity): VideoModel {

        val audioUrls = videoEntity.audioUrls
        val userLanguage = languageHelper.currentLanguageCode


        val audioUrl: String
        audioUrl = if (audioUrls.containsKey(userLanguage)) {
            audioUrls.getValue(userLanguage)
        } else {
            audioUrls.getValue(Settings.DEFAULT_LANGUAGE)
        }


        val videoUri = Uri.parse(videoEntity.videoUrl)
        val lastPath = videoUri.lastPathSegment
        val thumbUrl = videoEntity.videoUrl.replace(lastPath!!, videoEntity.thumbnail)


        return VideoModel(
            videoUrl = videoEntity.videoUrl,
            audioUrl = audioUrl,
            thumbnailUrl = thumbUrl,
            title = videoEntity.title
        )

    }

    fun map(videoEntities: List<VideoEntity>): List<VideoModel> {

        return videoEntities.map {
            map(it)
        }
    }

}