package aziz.sohail.videoplayer.data.datastore.local

import aziz.sohail.videoplayer.data.entity.VideoEntity
import io.reactivex.Single

interface IMockDataStore {
    fun getVideoList(): Single<List<VideoEntity>>
}