package aziz.sohail.videoplayer.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VideoModel(
    val videoUrl: String,
    val audioUrl: String,
    val thumbnailUrl: String,
    val title: String
) : Parcelable