package aziz.sohail.videoplayer.domain.interactor

import aziz.sohail.videoplayer.domain.IVideoRepository
import aziz.sohail.videoplayer.domain.executor.PostExecutionThread
import aziz.sohail.videoplayer.domain.executor.ThreadExecutor
import aziz.sohail.videoplayer.domain.interactor.base.ActivityLifecycleSingleUseCase
import aziz.sohail.videoplayer.domain.model.VideoModel
import com.trello.rxlifecycle2.android.ActivityEvent
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class GetVideosUseCase
@Inject constructor(
    private val videoRepository: IVideoRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread,
    activityEventObservable: Observable<ActivityEvent>
) : ActivityLifecycleSingleUseCase<List<VideoModel>>(
    threadExecutor,
    postExecutionThread,
    activityEventObservable
) {
    override fun buildUseCaseObservable(): Single<List<VideoModel>> {
        return videoRepository.getVideos()
    }
}