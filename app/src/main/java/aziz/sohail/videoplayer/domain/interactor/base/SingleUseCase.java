package aziz.sohail.videoplayer.domain.interactor.base;

import androidx.annotation.NonNull;

import aziz.sohail.videoplayer.domain.executor.PostExecutionThread;
import aziz.sohail.videoplayer.domain.executor.ThreadExecutor;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public abstract class SingleUseCase<T> {

  @NonNull private final ThreadExecutor threadExecutor;
  @NonNull private final PostExecutionThread postExecutionThread;

  private DisposableSingleObserver<T> subscription = new DefaultSingleSubscriber<T>();

  public SingleUseCase(
      @NonNull ThreadExecutor threadExecutor, @NonNull PostExecutionThread postExecutionThread
  ) {
    this.threadExecutor = threadExecutor;
    this.postExecutionThread = postExecutionThread;
  }

  @NonNull public abstract Single<T> buildUseCaseObservable();

  public void execute(@NonNull DisposableSingleObserver<T> useCaseSubscriber) {
    subscription = useCaseSubscriber;
    getSingle().subscribe(useCaseSubscriber);
  }

  public void unsubscribePreviousAndExecute(
      @NonNull DisposableSingleObserver<T> useCaseSubscriber
  ) {
    unsubscribe();
    execute(useCaseSubscriber);
  }

  @NonNull public Single<T> getSingle() {
    return this.buildUseCaseObservable()
        .subscribeOn(Schedulers.from(threadExecutor))
        .observeOn(postExecutionThread.getScheduler());
  }

  /**
   * Unsubscribes from current {@link Disposable}.
   */
  @NonNull public SingleUseCase<T> unsubscribe() {
    if (!subscription.isDisposed()) {
      subscription.dispose();
    }
    return this;
  }
}
