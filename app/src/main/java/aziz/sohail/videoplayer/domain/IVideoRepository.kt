package aziz.sohail.videoplayer.domain

import aziz.sohail.videoplayer.domain.model.VideoModel
import io.reactivex.Single

interface IVideoRepository {

    fun getVideos(): Single<List<VideoModel>>
}