package aziz.sohail.videoplayer.domain.interactor.base;

import io.reactivex.observers.DisposableSingleObserver;
import timber.log.BuildConfig;
import timber.log.Timber;

public class DefaultSingleSubscriber<T> extends DisposableSingleObserver<T> {

  private final boolean isLogErrorInRelease;

  public DefaultSingleSubscriber() {
    this(true);
  }

  public DefaultSingleSubscriber(boolean isLogErrorInRelease) {
    this.isLogErrorInRelease = isLogErrorInRelease;
  }

  @Override public void onSuccess(T t) {
    Timber.d("onSuccess %s", String.valueOf(t));
  }

  @Override public void onError(Throwable e) {
    if (BuildConfig.DEBUG || isLogErrorInRelease) {
      Timber.e(e, "onError");
    }
  }
}
