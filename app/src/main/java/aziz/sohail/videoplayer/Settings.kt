package aziz.sohail.videoplayer

class Settings {
    companion object {
        const val DEFAULT_LANGUAGE = "en"
        const val USER_AGENT_NAME = "Video Player"
    }
}