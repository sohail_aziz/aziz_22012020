package aziz.sohail.videoplayer.presentation.ui.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import aziz.sohail.videoplayer.presentation.ui.presenter.OnPlayingChangedListener
import aziz.sohail.videoplayer.presentation.viewmodel.VideoViewModel
import com.google.android.exoplayer2.upstream.DataSource
import javax.inject.Inject

class VideoListAdapter
@Inject constructor(
    private val playingChangedListener: OnPlayingChangedListener,
    private val dataSourceFactory: DataSource.Factory
) : RecyclerView.Adapter<VideoViewHolder>() {


    private val videoList = mutableListOf<VideoViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(VideoViewHolder.LAYOUT, parent, false)
        return VideoViewHolder(view, playingChangedListener, dataSourceFactory)
    }

    override fun getItemCount(): Int {
        return videoList.size
    }

    override fun onBindViewHolder(holder: VideoViewHolder, position: Int) {
        holder.bind(videoList[position])
    }

    override fun onViewAttachedToWindow(holder: VideoViewHolder) {
        super.onViewAttachedToWindow(holder)
    }

    override fun onViewDetachedFromWindow(holder: VideoViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.onViewDetached()
    }

    fun setData(videos: List<VideoViewModel>) {
        videoList.clear()
        videoList.addAll(videos)
        notifyDataSetChanged()
    }

    fun pauseVideo(lastPosition: Int) {
        videoList[lastPosition].play = false
        notifyItemChanged(lastPosition)
    }


    fun pauseIfPlaying() {
        val currentPlayIndex = videoList.indexOfFirst {
            it.play
        }

        videoList[currentPlayIndex].play = false
        notifyItemChanged(currentPlayIndex)

    }
}