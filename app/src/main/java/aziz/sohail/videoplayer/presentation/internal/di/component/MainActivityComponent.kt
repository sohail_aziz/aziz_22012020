package aziz.sohail.videoplayer.presentation.internal.di.component

import aziz.sohail.videoplayer.presentation.internal.di.PerActivity
import aziz.sohail.videoplayer.presentation.internal.di.module.MainActivityModule
import aziz.sohail.videoplayer.presentation.ui.view.MainActivity
import dagger.Subcomponent


@PerActivity
@Subcomponent(modules = [MainActivityModule::class])
interface MainActivityComponent {


    //MainActivityComponent builder
    @Subcomponent.Builder
    interface Builder {
        fun mainActivityModule(module: MainActivityModule): Builder

        fun build(): MainActivityComponent
    }

    fun inject(activity: MainActivity)


}