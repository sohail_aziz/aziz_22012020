package aziz.sohail.videoplayer.presentation.internal.di.module

import aziz.sohail.videoplayer.data.VideoRepository
import aziz.sohail.videoplayer.data.datastore.local.IMockDataStore
import aziz.sohail.videoplayer.data.datastore.local.MockDataStore
import aziz.sohail.videoplayer.domain.IVideoRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class VideoModule {

    @Singleton
    @Provides
    fun provideVideoRepository(videoRepository: VideoRepository): IVideoRepository = videoRepository

    @Singleton
    @Provides
    fun provideVideoLocalDataStore(localDataStore: MockDataStore): IMockDataStore = localDataStore

}