package aziz.sohail.videoplayer.presentation.internal.di.module

import android.content.Context
import aziz.sohail.videoplayer.data.executor.JobExecutor
import aziz.sohail.videoplayer.domain.executor.PostExecutionThread
import aziz.sohail.videoplayer.domain.executor.ThreadExecutor
import aziz.sohail.videoplayer.presentation.base.UIThread
import aziz.sohail.videoplayer.presentation.ui.VideoApplication
import dagger.Module
import dagger.Provides
import aziz.sohail.videoplayer.presentation.internal.di.component.MainActivityComponent
import javax.inject.Singleton


@Module(subcomponents = [MainActivityComponent::class])
class ApplicationModule {

    @Provides
    @Singleton
    fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor = jobExecutor

    @Provides
    @Singleton
    fun providePostExecutionThread(uiThread: UIThread): PostExecutionThread = uiThread

    @Provides
    @Singleton
    fun provideApplicationContext(application: VideoApplication): Context =
        application.applicationContext

}