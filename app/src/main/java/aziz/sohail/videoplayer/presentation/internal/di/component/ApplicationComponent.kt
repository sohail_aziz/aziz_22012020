package aziz.sohail.videoplayer.presentation.internal.di.component

import aziz.sohail.videoplayer.presentation.internal.di.module.ApplicationModule
import aziz.sohail.videoplayer.presentation.internal.di.module.NetworkModule
import aziz.sohail.videoplayer.presentation.internal.di.module.VideoModule
import aziz.sohail.videoplayer.presentation.ui.VideoApplication
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [ApplicationModule::class, NetworkModule::class, VideoModule::class])
interface ApplicationComponent {

    //injection point
    fun inject(application: VideoApplication)

    //subComponents
    fun mainActivityComponentBuilder(): MainActivityComponent.Builder

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun currencyConverterApplication(application: VideoApplication): Builder

        fun build(): ApplicationComponent
    }


}