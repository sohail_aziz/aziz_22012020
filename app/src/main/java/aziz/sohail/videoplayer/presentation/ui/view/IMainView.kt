package aziz.sohail.videoplayer.presentation.ui.view

import aziz.sohail.videoplayer.presentation.viewmodel.VideoViewModel
import io.reactivex.internal.util.AtomicThrowable

interface IMainView {

    fun onErrorLoadingVideos(throwable: Throwable)

    fun onVideosLoaded(videos: List<VideoViewModel>)

    fun pauseLastVideo(lastPosition: Int)

}