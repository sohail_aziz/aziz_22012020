package aziz.sohail.videoplayer.presentation.ui.view

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import aziz.sohail.videoplayer.R
import aziz.sohail.videoplayer.Settings
import aziz.sohail.videoplayer.domain.model.VideoModel
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.MergingMediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.activity_video_player.*


class VideoPlayerActivity : AppCompatActivity() {

    private lateinit var player: SimpleExoPlayer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_player)


        if (intent.hasExtra(ARG_VIDEO_MODEL)) {


            val videoModel: VideoModel = intent.getParcelableExtra(ARG_VIDEO_MODEL)!!

            player = SimpleExoPlayer.Builder(this).build()
            player_view.player = player

            val dataSourceFactory: DataSource.Factory = DefaultDataSourceFactory(
                this,
                Util.getUserAgent(this, Settings.USER_AGENT_NAME)
            )

            val videoSource: MediaSource = ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(
                    Uri.parse(videoModel.videoUrl)
                )

            val audioSource = ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(videoModel.audioUrl))

            val mergedSource = MergingMediaSource(videoSource, audioSource)

            player.prepare(mergedSource)

            player.playWhenReady = true
        }

    }

    override fun onStop() {
        super.onStop()
        player.stop()
        player.release()
    }

    companion object {

        private const val ARG_VIDEO_MODEL = "ARG_VIDEO_MODEL"

        fun getCallingIntent(context: Context, videoModel: VideoModel): Intent {

            val intent = Intent(context, VideoPlayerActivity::class.java)

            val args = Bundle()
            args.putParcelable(ARG_VIDEO_MODEL, videoModel)

            intent.putExtras(args)

            return intent
        }
    }

}
