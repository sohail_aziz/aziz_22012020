package aziz.sohail.videoplayer.presentation.ui

import android.app.Application
import android.content.Context
import aziz.sohail.videoplayer.presentation.internal.di.component.ApplicationComponent
import aziz.sohail.videoplayer.presentation.internal.di.component.DaggerApplicationComponent
import timber.log.Timber

class VideoApplication : Application() {

    lateinit var applicationComponent: ApplicationComponent
    override fun onCreate() {
        super.onCreate()
        applicationComponent = DaggerApplicationComponent
            .builder()
            .currencyConverterApplication(this)
            .build()

        Timber.plant(Timber.DebugTree())
    }

    fun getComponent(context: Context): ApplicationComponent {
        return applicationComponent;
    }

    companion object {
        fun getComponent(context: Context): ApplicationComponent {
            return (context as VideoApplication).applicationComponent;
        }

        fun getApplication(context: Context): VideoApplication {
            return (context as VideoApplication)
        }
    }
}