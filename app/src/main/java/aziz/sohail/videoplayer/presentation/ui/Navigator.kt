package aziz.sohail.videoplayer.presentation.ui

import aziz.sohail.videoplayer.domain.model.VideoModel
import aziz.sohail.videoplayer.presentation.ui.view.VideoPlayerActivity
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity
import javax.inject.Inject

class Navigator
@Inject constructor(private val activity: RxAppCompatActivity) {

    fun startFullScreenVideoActivity(videoModel: VideoModel) {
        activity.startActivity(
            VideoPlayerActivity.getCallingIntent(
                context = activity,
                videoModel = videoModel
            )
        )
    }
}