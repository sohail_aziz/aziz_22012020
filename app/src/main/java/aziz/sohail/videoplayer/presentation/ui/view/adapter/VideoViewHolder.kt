package aziz.sohail.videoplayer.presentation.ui.view.adapter

import android.net.Uri
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import aziz.sohail.videoplayer.R
import aziz.sohail.videoplayer.presentation.ui.presenter.OnPlayingChangedListener
import aziz.sohail.videoplayer.presentation.ui.view.hide
import aziz.sohail.videoplayer.presentation.ui.view.show
import aziz.sohail.videoplayer.presentation.viewmodel.VideoViewModel
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.MergingMediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.squareup.picasso.Picasso


class VideoViewHolder(
    itemView: View,
    private val playingChangeListener: OnPlayingChangedListener,
    private val dataSourceFactory: DataSource.Factory
) : RecyclerView.ViewHolder(itemView) {

    @BindView(R.id.text_view_title)
    lateinit var title: TextView
    @BindView(R.id.button_pause)
    lateinit var buttonPause: ImageButton
    @BindView(R.id.button_play)
    lateinit var buttonPlay: ImageButton
    @BindView(R.id.player_view)
    lateinit var playerView: PlayerView
    @BindView(R.id.image_view_thumbnail)
    lateinit var thumbnailImage: ImageView


    private val player: SimpleExoPlayer
    private lateinit var viewModel: VideoViewModel


    init {
        ButterKnife.bind(this, itemView)
        player = SimpleExoPlayer.Builder(itemView.context).build()
        playerView.player = player
    }


    fun bind(videoViewModel: VideoViewModel) {

        this.viewModel = videoViewModel


        title.text = videoViewModel.videoModel.title

        Picasso.with(itemView.context)
            .load(viewModel.videoModel.thumbnailUrl)
            .placeholder(R.drawable.ic_place_holder)
            .into(thumbnailImage)


        val videoSource: MediaSource = ProgressiveMediaSource.Factory(dataSourceFactory)
            .createMediaSource(Uri.parse(videoViewModel.videoModel.videoUrl))

        val audioSource: MediaSource = ProgressiveMediaSource.Factory(dataSourceFactory)
            .createMediaSource(Uri.parse(videoViewModel.videoModel.audioUrl))

        val mergedMediaSource = MergingMediaSource(videoSource, audioSource)

        player.prepare(mergedMediaSource)

        if (viewModel.play) {
            playVideo()
        } else {
            pauseVideo()
        }
    }

    private fun playVideo() {
        //play
        viewModel.play = true
        player.apply {
            playWhenReady = true
            seekTo(viewModel.currentWindowIndex, viewModel.playBackPosition)
        }

        //show pause button
        showPauseButton()
    }

    private fun pauseVideo() {
        viewModel.currentWindowIndex = player.currentWindowIndex
        viewModel.playBackPosition = player.currentPosition

        //pause
        viewModel.play = false
        player.playWhenReady = false

        //show play button
        showPlayButton()
    }

    private fun showPauseButton() {
        buttonPause.show()

        buttonPlay.hide()
        thumbnailImage.hide()

    }

    private fun showPlayButton() {
        buttonPlay.show()
        buttonPause.hide()

    }

    fun onViewDetached() {
        pauseVideo()
    }

    @OnClick(R.id.button_pause)
    fun onPauseClicked() {
        pauseVideo()
    }


    @OnClick(R.id.button_play)
    fun onPlayClicked() {
        playingChangeListener.onPlayingChanged(true, adapterPosition)
        playVideo()

    }

    @OnClick(R.id.button_full_screen)
    fun onFullScreenClicked() {
        playingChangeListener.onPlayInFullScreen(viewModel.videoModel)
    }

    companion object {
        const val LAYOUT = R.layout.layout_list_item_video
    }


}

