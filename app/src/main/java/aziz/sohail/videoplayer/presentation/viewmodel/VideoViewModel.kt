package aziz.sohail.videoplayer.presentation.viewmodel

import aziz.sohail.videoplayer.domain.model.VideoModel

data class VideoViewModel(
    val videoModel: VideoModel,
    var play: Boolean = false,
    var currentWindowIndex: Int = 0,
    var playBackPosition: Long = 0L
)