package aziz.sohail.videoplayer.presentation.ui.presenter

import aziz.sohail.videoplayer.domain.interactor.GetVideosUseCase
import aziz.sohail.videoplayer.domain.interactor.base.DefaultSingleSubscriber
import aziz.sohail.videoplayer.domain.model.VideoModel
import aziz.sohail.videoplayer.presentation.ui.Navigator
import aziz.sohail.videoplayer.presentation.ui.view.IMainView
import aziz.sohail.videoplayer.presentation.viewmodel.VideoViewModel
import com.trello.rxlifecycle2.android.ActivityEvent
import javax.inject.Inject

class MainPresenter
@Inject constructor(
    getVideosUseCase: GetVideosUseCase,
    private val view: IMainView,
    private val navigator: Navigator
) : IMainPresenter, OnPlayingChangedListener {

    private var lastPlayingPos = UNDEFINED

    init {

        getVideosUseCase.unsubscribeOn(ActivityEvent.DESTROY)
            .execute(object : DefaultSingleSubscriber<List<VideoModel>>() {
                override fun onSuccess(t: List<VideoModel>) {
                    super.onSuccess(t)

                    view.onVideosLoaded(t.map {
                        VideoViewModel(it)
                    })
                }

                override fun onError(e: Throwable) {
                    super.onError(e)
                    view.onErrorLoadingVideos(e)
                }

            })
    }

    override fun onPlayInFullScreen(videoModel: VideoModel) {
        navigator.startFullScreenVideoActivity(videoModel)
    }

    override fun onPlayingChanged(isPlaying: Boolean, position: Int) {

        if (lastPlayingPos != UNDEFINED) {
            view.pauseLastVideo(lastPlayingPos)
        }

        //update last playing pos
        if (isPlaying) {
            lastPlayingPos = position
        }
    }

    companion object {
        private const val UNDEFINED = -1
    }
}