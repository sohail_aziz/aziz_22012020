package aziz.sohail.videoplayer.presentation.ui.view

import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import aziz.sohail.videoplayer.R
import aziz.sohail.videoplayer.presentation.internal.di.component.MainActivityComponent
import aziz.sohail.videoplayer.presentation.internal.di.module.MainActivityModule
import aziz.sohail.videoplayer.presentation.ui.VideoApplication
import aziz.sohail.videoplayer.presentation.ui.presenter.IMainPresenter
import aziz.sohail.videoplayer.presentation.ui.view.adapter.VideoListAdapter
import aziz.sohail.videoplayer.presentation.viewmodel.VideoViewModel
import butterknife.BindView
import butterknife.ButterKnife
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity
import timber.log.Timber
import javax.inject.Inject

class MainActivity : RxAppCompatActivity(), IMainView {

    @Inject
    lateinit var presenter: IMainPresenter

    @Inject
    lateinit var adapter: VideoListAdapter

    @BindView(R.id.recycler_view_videos)
    lateinit var recyclerView: RecyclerView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)
        getInjector().inject(this)
        initView()

    }

    private fun initView() {

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
    }

    private fun getInjector(): MainActivityComponent {
        return VideoApplication.getComponent(context = applicationContext)
            .mainActivityComponentBuilder()
            .mainActivityModule(module = MainActivityModule(activity = this))
            .build();
    }

    override fun onVideosLoaded(videos: List<VideoViewModel>) {
        adapter.setData(videos)
    }

    override fun onErrorLoadingVideos(throwable: Throwable) {
        Toast.makeText(this, throwable.localizedMessage, Toast.LENGTH_SHORT).show()
    }

    override fun pauseLastVideo(lastPosition: Int) {
        adapter.pauseVideo(lastPosition)
    }

    override fun onPause() {
        super.onPause()
        adapter.pauseIfPlaying()
    }

}
