package aziz.sohail.videoplayer.presentation.internal.di.module

import android.content.Context
import aziz.sohail.videoplayer.Settings
import aziz.sohail.videoplayer.presentation.internal.di.PerActivity
import aziz.sohail.videoplayer.presentation.ui.presenter.IMainPresenter
import aziz.sohail.videoplayer.presentation.ui.presenter.MainPresenter
import aziz.sohail.videoplayer.presentation.ui.presenter.OnPlayingChangedListener
import aziz.sohail.videoplayer.presentation.ui.view.IMainView
import aziz.sohail.videoplayer.presentation.ui.view.MainActivity
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import dagger.Module
import dagger.Provides


@PerActivity
@Module
class MainActivityModule(private val activity: MainActivity) :
    BaseActivityModule(activity = activity) {

    @PerActivity
    @Provides
    fun provideMainView(): IMainView = activity

    @PerActivity
    @Provides
    fun provideMainPresenter(presenter: MainPresenter): IMainPresenter = presenter


    @PerActivity
    @Provides
    fun provideOnPlayingChangedListener(presenter: MainPresenter): OnPlayingChangedListener =
        presenter

    @PerActivity
    @Provides
    fun provideDataSourceFactory(context: Context): DataSource.Factory {
        return DefaultDataSourceFactory(
            context,
            Util.getUserAgent(context, Settings.USER_AGENT_NAME)
        )
    }

}