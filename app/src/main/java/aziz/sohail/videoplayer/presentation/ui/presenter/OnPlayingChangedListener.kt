package aziz.sohail.videoplayer.presentation.ui.presenter

import aziz.sohail.videoplayer.domain.model.VideoModel

interface OnPlayingChangedListener {

    fun onPlayingChanged(isPlaying: Boolean, position: Int)

    fun onPlayInFullScreen(videoModel: VideoModel)

}